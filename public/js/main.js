var feedreader = angular.module('feedreader', ['infinite-scroll']);

feedreader.filter('formatTime', function() {
    return function(value) {
        if(!value) return value;
        var d =  new Date(value);
        return d.getDate() + "/" + d.getMonth() + "/" + d.getFullYear();
    };
});

feedreader.filter('summarize', function() {
    return function(value) {
        var summary = value.split(/\s+/).slice(0, 50).join(' ');
        summary += '...';
        return summary;
    }
});

feedreader.factory('ArticleService', ['$http', function($http) {
    return {
        fetchAll: function(pos) {
            var url;
            if(typeof pos === typeof undefined)
                url = '/api/articles';
            else
                url = '/api/articles?pos='+pos;

            return $http.get(url);
        },
        fetch: function(id) {
            return $http.get('/api/article/'+id);
        },
        annotate: function(id, pos, text, quote) {
           var params = {id: id, pos: pos, text: text, quote: quote};
           return $http.post('/api/annotate', params);
        },
        deleteAnnotation: function(id, aid) {
            return $http.delete('/api/annotations/'+id+'/'+aid);
        }
    }
}]);

feedreader.controller('articleListController', ['$scope', 'ArticleService', function($scope, ArticleService) {

    function fetchArticles(args) {
        ArticleService.fetchAll(args.pos).then(function(request) {
            $scope.articles = $scope.articles.concat(request.data.articles);
            $scope.position = request.data.position;
            if(typeof args.callback != typeof undefined) args.callback({error: true});
        }, function(request) {
            showError('Error: An error occurred when trying to fetch articles');
            if(typeof args.callback != typeof undefined) args.callback({error: true});
        });
    }

    function showError(msg) {
        alert(msg);
    }

    function init() {
        $scope.articles = [];
        $scope.position = 0;
        $scope.isLoading = false;
        $scope.isLoadingInline = false;
        $scope.curArticle = {};

        $scope.loadMoreArticles = function() {
            if($scope.position != -1) {
                $scope.isLoadingInline = false;
                fetchArticles({pos:$scope.position, callback: function() { $scope.isLoadingInline = false; }});
            }
        };
        $scope.showArticle = function(id) {
            $('#article-modal').modal('show');
                ArticleService.fetch(id).then(function(request) {
                        $scope.curArticle = request.data;
                        $('#article-content').text($scope.curArticle.description);
                        setTimeout(function() {
                            var annotations = $scope.curArticle.annotations;
                            if(annotations.length > 0) {
                                for(a in annotations) {
                                    if(annotations[a] == null) continue;
                                    wrapAnnotation(annotations[a].id, annotations[a].pos.start, annotations[a].pos.end, annotations[a].text, annotations[a].quote);
                                }
                            }
                        }, 500);
                }, function(request) {
                        showError('Error: Cannot fetch article');
                });
        };

        $scope.saveAnnotation = function() {
            var text = $scope.annotationInput;
           /* var range = rangy.getSelection().getRangeAt(0);
            var serialized_range = rangy.serializeRange(range);*/
            rangy.restoreSelection(getPreviousSelection());
            var pos = getSelectedTextPosition();
            ArticleService.annotate($scope.curArticle._id, pos, text, window.getSelection().toString()).then(function(response) {
                hideAnnotationInput();
                $scope.showArticle($scope.curArticle._id);
            }, function(response) { showError(response.errorMessage)});
        };
    }

    function refresh() {
        $scope.isLoading = true;
        fetchArticles({callback: function() { $scope.isLoading = false; }});
    }

    init();
    refresh();
}]);