/**
 * Created by bhaskar on 4/5/14.
 */
function getSelectionText() {
    var text = "";
    var start = 0, end = 0;
    var sel, range, priorRange;
    if (typeof window.getSelection != "undefined") {
        range = window.getSelection().getRangeAt(0);
        priorRange = range.cloneRange();
        priorRange.selectNodeContents()
        text = window.getSelection().toString();
    } else if (document.selection && document.selection.type != "Control") {
        text = document.selection.createRange().text;
    }
    return text;
}

function getSelectionCharOffsetsWithin(element) {
    var start = 0, end = 0;
    var sel, range, priorRange;
    if (typeof window.getSelection != "undefined") {
        range = window.getSelection().getRangeAt(0);
        priorRange = range.cloneRange();
        priorRange.selectNodeContents(element);
        priorRange.setEnd(range.startContainer, range.startOffset);
        start = priorRange.toString().length;
        end = start + range.toString().length;
    } else if (typeof document.selection != "undefined" &&
        (sel = document.selection).type != "Control") {
        range = sel.createRange();
        priorRange = document.body.createTextRange();
        priorRange.moveToElementText(element);
        priorRange.setEndPoint("EndToStart", range);
        start = priorRange.text.length;
        end = start + range.text.length;
    }
    return {
        start: start,
        end: end
    };
}

function hideAnnotationInput() {
    $('#annotate-btn').fadeOut();
    $('#annotation-input').fadeOut();
    $('#annotate-text').val('');
}

function wrapText(html) {
    var div = document.createElement('div'),
        frag = document.createDocumentFragment();

    div.innerHTML = html;

    while (div.firstChild) {
        frag.appendChild( div.firstChild );
    }

    return frag;
}

function wrapAnnotation(id, start, end,  text, quote) {
    p = document.getElementById('article-content');
    nodes = getTextNodesIn(p);
    var node;
    var flag = false;
    for(n in nodes) {
        if(nodes[n].textContent.substr(start, (end-start)) == quote) {
            node = nodes[n];
            flag = true;
            break;
        }
    }
    if(flag) {
        p.normalize();
        newChild = wrapText(node.nodeValue.replace(node.nodeValue.slice(start, end), '<span class="annotation" data-id="'+id+'" data-text="'+text+'">$&</span>'));
        p.replaceChild(newChild, node);
    }
    return flag;

}

function getTextNodesIn(node, includeWhitespaceNodes) {
    if(typeof includeWhitespaceNodes == "undefined") includeWhitespaceNodes = true;
    var textNodes = [], nonWhitespaceMatcher = /\S/;


    function getTextNodes(node) {
        if (node.nodeType == 3) {
            if (includeWhitespaceNodes || !nonWhitespaceMatcher.test(node.nodeValue)) {
                textNodes.push(node);
            }
        } else {
            for (var i = 0, len = node.childNodes.length; i < len; ++i) {
                getTextNodes(node.childNodes[i]);
            }
        }
    }

    getTextNodes(node);
    return textNodes;
}

function getSelectedTextPosition() {
    var selection = window.getSelection();
    var start = selection.anchorOffset;
    var end = selection.focusOffset;
    return {start: start, end:end};
}

var selection;

function getPreviousSelection() {
    return selection;
}

$(document).ready(function() {
    rangy.init();
    $('#annotate-btn').hide();
    $('#annotate-btn').click(function() {
        var pos = $(this).position();
        var width = $(this).width();
        var height = $(this).height();
        //var newPosY = pos.top + height + 20;
        //var newPosX = (pos.left + (pos.left + width)) / 2;
        var newPosY = (((pos.top + height)+pos.top)/2) - $('#annotation-input .arrow').height / 2;
        var inputPos = $('#annotation-input').position();
        var arrowPos = $('#annotation-input .arrow').position();
        $('#annotation-input').css({position:"absolute", top:pos.top - 46, left: pos.left + width + 30}).toggle();
        scope = angular.element('#article-container').scope;
        /*scope.$apply(function() {
            scope.selectedText = window.getSelection().toString();
        });*/
        selection = rangy.saveSelection();
        //$('#annotation-text').focus();
        //rangy.restoreSelection(selection);
    });

    var selection_handled = false;
    var selectionClass = rangy.createCssClassApplier("selection");

    $(document).on('mouseup', '#article-content:not(.disable-events)', function(event) {
       /* var sel = getSelectionCharOffsetsWithin(this);
        if(sel.start == 0 && sel.end == 0) return;*/
        $this = $(this);
        $this.addClass('disable-events');
        if(selection_handled) {
            //selectionClass.toggleSelection();
            //rangy.getSelection().removeAllRanges();
            $('#annotate-btn').fadeOut();
            $('#annotation-input').fadeOut();
            selection_handled = false;
        }
        else if(rangy.getSelection().rangeCount) {
            //selectionClass.toggleSelection();
            var selection = $('.selection');
            var pos = selection.position();
            var annotate_btn = $('#annotate-btn');
            //annotate_btn.css({position:"absolute",top:pos.top - annotate_btn.height() - 20, left: pos.left}).fadeIn();
            var parentOffset = $(this).parent().offset();
            var offsetX = event.pageX - parentOffset.left;
            var offsetY = event.pageY - parentOffset.top;
            annotate_btn.css({position:"absolute",top:offsetY, left: offsetX + 10}).fadeIn();
            //selection.replaceWith(selection.contents());
            selection_handled = true;
        }
        $this.removeClass('disable-events');
    });

    $("#annotation-container").hover(function() { $(this).addClass('visible'); }, function() { $(this).removeClass('visible'); $(this).fadeOut()})
    $(document).on('mouseover', '.annotation', function(event) {
        var pos = $(this).position();
        var text = $(this).attr('data-text');
        var id = $(this).attr('data-id');
        var scope = angular.element('#annotation-container').scope();
        scope.$apply(function() {
            scope.annotation = {id: id, text: text};
        });
        $('#annotation-container:not(.visible)').css({position:"absolute", top: pos.top+20, left: pos.left}).fadeIn();
    });

    $(document).on('mouseout', '.annotation', function(event) {
        setTimeout(function() {$('#annotation-container:not(.visible)').fadeOut()}, 100);
    });
});