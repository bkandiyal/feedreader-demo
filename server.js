var express = require('express');
var app = express();
var articles = require('./articles');

var API_PREFIX='/api';

app.use(express.json());
app.use(express.urlencoded());
app.use('/', express.static(__dirname + '/public'));

app.get(API_PREFIX+'/articles', articles.findAll);

app.get(API_PREFIX+'/article/:id', articles.findById);

app.post(API_PREFIX+'/annotate', articles.annotate);

app.delete(API_PREFIX+'/annotations/:id/:aid', articles.deleteAnnotation);

app.listen(3000, function() { console.log('Server started'); });