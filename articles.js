var mongo = require("mongodb");
var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;
var server = new Server('localhost', 27017, {auto_reconnect: true});
db = new Db('articles', server, {safe: false});

db.open(function(err, db) {
    if(!err) {
        console.log("Established connection with database");
        db.collection('articles', {strict:true}, function(err, collection) {
            if(err) {
                console.log('The collection articles does not exist. Please run the \'import_articles.js\' ' +
                    'application to import the database');
                throw err;
            }
        });
    }
    else {
        throw err;
    }
});

exports.findAll = function(req, res) {
    db.collection('articles', function(err, collection) {
        var pos = (typeof req.query.pos === typeof undefined)? 0 : parseInt(req.query.pos)  ;
        collection.count(function(err, count) {
            if(err) res.send({'error': 'Error counting items in the collection'});
            collection.find({}, {description: false, sort:[['date_published','desc']]}).skip(pos).limit(40).toArray(function(err, items) {
                if(err) res.send({'error': 'Error fetching collection'});
                else {
                    for(i in items) {
                        item = items[i];
                        item.description = item.description.replace(/<(?:.|\n)*?>/gm, ''); // Strip HTML tags from description
                        item.summary = item.description.split(/\s+/).slice(0, 100).join(' '); // Take first 100 words of description for summary
                        item.summary += '...';
                        delete item.description; // Remove the description since we don't want to send huge data to the browser
                    }
                    pos = pos + items.length;
                    if(pos >= count) {
                        pos = -1;
                    }
                    ret = {articles: items, position: pos};
                    res.send(ret);
                }
            });
        });
    });
};

exports.findById = function(req, res) {
    var id = req.params.id;
    db.collection('articles', function(err, collection) {
        collection.findOne({'_id': new BSON.ObjectID(id)}, function(err, item) {
            if(err) res.send({'error': 'Error fetching collection'}, 401);
            else res.send(item);
        });
    });
};

exports.annotate = function(req, res) {
    var id = req.body.id;
    var pos = req.body.pos;
    var quote = req.body.quote;
    var text = req.body.text;
    db.collection('articles', function(err, collection) {
        collection.findOne({'_id': new BSON.ObjectID(id)}, function(err, item) {
            if(err) res.send({'error': 'Unknown article ID'}, 404);
            else {
                var annotation = {id:Math.round(Math.random() * 10000), pos: pos, text: text, quote:quote};
                item.annotations.push(annotation);
                collection.save(item);
                res.send({id: annotation.id});
            }
        })
    });
};

exports.deleteAnnotation = function(req, res) {
    var id = req.params.id;
    var aid = req.params.aid;
    db.collection('articles', function(err, collection) {
        collection.findOne({'_id': new BSON.ObjectID(id)}, function(err, item) {
            if(err) res.send({'error': 'Unknown article ID'}, 404);
            else {
                for(a in item.annotations) {
                    if(item.annotations[a] == null) continue;
                    if(item.annotations[a].id == aid) {
                        delete item.annotations[a];
                        collection.save(item);
                        res.send({});
                        return;
                    }
                }
                res.send({'error':'No such annotation'}, 404);
            }
        })
    });
};
