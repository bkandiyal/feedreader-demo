var MongoClient = require('mongodb').MongoClient;
var format = require('util').format;
console.log('Parsing article_dump.json');
var articles = require('./article_dump.json');

console.log('Connecting to MongoDB');

function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

MongoClient.connect('mongodb://127.0.0.1:27017/articles', function(err, db) {
	if(err) throw err;
	
	var collection = db.collection('articles');
	console.log("Importing " + articles.length + " articles");
	for(a in articles) {
		article = articles[a];
        article.headline = article.description.split(/\s+/).slice(0,5).join(' ');
        article.headline = article.headline.replace(/<(?:.|\n)*?>/gm, '');
        article.date_published = randomDate(new Date(2013, 0, 1), new Date()).getTime();
        article.description = article.description.trim();
        article.annotations = [];
		collection.insert(article, function(err, docs) {
			if(err) throw err;
		});
	}
	console.log('Done'); 
	db.close();
	process.exit(0);
});
