NodeJS Feed Reader Demo
=======================

This project is a demo application showcasing the functionality of a Feedly like feed reader developed in Node.JS + MongoDB + AngularJS.

Features
========
* Infinite scrollable list of feeds
* Ability to annotate text of the feeds

Technology Used
===============

* Frontend: Bootstrap, jQuery and AngularJS
* Backend: Node.JS + Express.JS + MongoDB

How To Install
==============

* Clone the git repository
* Run the command **node import_articles** - This will import the dummy feeds into MongoDB
* Run **node server** to start the server
* The server runs on port 3000, so open up http://localhost:3000 in your browser to access the application